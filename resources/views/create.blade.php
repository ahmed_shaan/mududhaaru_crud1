<nav>
    <a href="{{ route('home') }}">Home</a>
</nav>
<x-layout title="Create">
    <form action="/store" method="post">
        @csrf
        <label for="name">Name</label>
        <input type="text" value="{{ old('name') }}" name="name" id="name">
        <label for="address">Address</label>
        <input type="text" value="{{ old('address') }}" name="address" id="address">
        <label for="dob">Date of Birth</label>
        <input type="date" value="{{ old('dob') }}" name="dob" id="dob">
        <br>
        <label for="gender">Gender</label>
        <select name="gender" id="gender">
            <option value="male" {{ old('gender') == 'male' ? 'selected' : '' }}>Male</option>
            <option value="female" {{ old('gender') == 'female' ? 'selected' : '' }}>Female</option>
        </select>
        <br>
        <label for="vaccinated" name="vaccinated">Vaccinated?</label>
        <input type="radio" {{ old('vaccinated') == 1 ? 'checked' : '' }} name="vaccinated" value="1" />Yes
        <input type="radio" {{ old('vaccinated') == 0 ? 'checked' : '' }} name="vaccinated" value="0" />No
        <br>
        <label for="type_of_vac">Type?</label>
        <input type="checkbox" {{ old('type_of_vac') == 'Pfiezer' ? 'checked' : '' }} name="type_of_vac"
            value="Pfiezer" /> Pfiezer
        <input type="checkbox" {{ old('type_of_vac') == 'Covishield' ? 'checked' : '' }} name="type_of_vac"
            value="Covishield" /> Covishield
        <br>
        <button type="submit">Submit</button>
    </form>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
    @endif

    <hr>

</x-layout>
