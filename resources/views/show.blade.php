<nav>
    <a href="{{ route('home') }}">Home</a>
</nav>
<x-layout title="Show">
    <h2>name: {{ $patient->name }}</h2>
    <h2>address: {{ $patient->address }}</h2>
    <h2>dob: {{ $patient->dob }}</h2>
    <h2>vacinated: {{ $patient->vaccinated == 1 ? 'Yes' : 'No'  }}</h2>
    <h2>type: {{ $patient->type_of_vac }}</h2>
</x-layout>
