<x-layout title="Tables">
    <nav>
        @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
        <a href="{{ route('create') }}">Create New</a>
    </nav>
    <table>
        <thead>
            <th>Name</th>
            <th>Address</th>
            <th>Date of Birth</th>
            <th>Gender</th>
            <th>Vaccinated</th>
            <th>Type</th>
            <th>Action</th>
        </thead>
        <tbody>
            @if ($patients->count())
                @foreach ($patients as $patient )
                    <tr>
                        <td>
                            {{ $patient->name }}
                        </td>
                        <td>
                            {{ $patient->address }}
                        </td>
                        <td>
                            {{ $patient->dob }}
                        </td>
                        <td>
                            {{ $patient->gender }}
                        </td>
                        <td>
                            {{ $patient->vaccinated === 1 ? 'yes' : 'no' }}
                        </td>
                        <td>
                            {{ $patient->type_of_vac }}
                        </td>
                         <td>
                            <a href="show/{{ $patient->id }}">View</a> <a href="edit/{{ $patient->id }}">Edit</a> |
                            <form action="{{ $patient->id }}" method="post" >
                                <button type="submit">Delete</button>
                                @csrf
                                @method('DELETE')
                            </form>
                        </td>
                    </tr>
                @endforeach
            @else
                There is not Data
            @endif
        </tbody>
    </table>
        {{ $patients->links() }}
<br>
<hr>

</x-layout>
