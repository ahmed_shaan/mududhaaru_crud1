<nav>
    <a href="{{ route('home') }}">Home</a>
</nav>
<x-layout title="Edit">
    <form action="/update/{{ $patient->id }}" method="post">
        @csrf
        @method('PUT')

        <label for="name">Name</label>
        <input type="text" value="{{ $patient->name }}" name="name" id="name">
        <label for="address">Address</label>
        <input type="text" name="address" value="{{ $patient->address }}" id="address">
        <label for="dob">Date of Birth</label>
        <input type="date" value="{{ $patient->dob }}" name="dob" id="dob">
        <br>
        <label for="gender">Gender</label>
        <select name="gender" id="gender">
            <option value="male" {{ $patient->gender == "male" ? 'selected' : '' }}>Male</option>
            <option value="female" {{ $patient->gender == "female" ? 'selected' : '' }}>Female</option>
        </select>
        <br>
        <label for=" vaccinated" name="vaccinated">Vaccinated?</label>
                <input {{ $patient->vaccinated == 1 ? 'checked' : '' }}  type="radio" name="vaccinated" value="1" />Yes
                <input {{ $patient->vaccinated == 0 ? 'checked' : '' }}  type="radio" name="vaccinated" value="0" />No
                <br>
                <label for="type_of_vac">Type?</label>
                <input  {{ $patient->type_of_vac == "Pfiezer" ? 'checked' : '' }} type="checkbox" name="type_of_vac" value="Pfiezer"/> Pfiezer
                <input  {{ $patient->type_of_vac == "Covishield" ? 'checked' : '' }} type="checkbox" name="type_of_vac" value="Covishield" />
                Covishield
                <br>
                <button type="submit">Submit</button>
    </form>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
            @endif

                    @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
</x-layout>
