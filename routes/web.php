<?php

use App\Http\Controllers\PatientsController;
use Illuminate\Support\Facades\Route;


Route::get('/', [PatientsController::class, 'index'])->name('home');
Route::get('/create', [PatientsController::class, 'create'])->name('create');
Route::get('/show/{patient}', [PatientsController::class, 'show']);
Route::post('/store', [PatientsController::class, 'store']);
Route::get('edit/{patient}', [PatientsController::class, 'edit'])->name('edit');
Route::put('update/{patient}', [PatientsController::class, 'update'])->name('update');
Route::delete('/{patient}', [PatientsController::class, 'destroy']);
