<?php

namespace App\Http\Controllers;

use App\Http\Requests\PatientStoreRequest;
use App\Models\Patients;
use Illuminate\Http\Request;

class PatientsController extends Controller
{
    public function index()
    {
        $patients = Patients::paginate(5);
        return view('index', compact('patients'));
    }

    public function create()
    {
        return view('create');
    }

    public function store(PatientStoreRequest $request)
    {
        $validatedAttributes = $request->validated();

        Patients::create($validatedAttributes);

        return redirect()->route('home')->with('status', 'Data created');
    }

    public function show(Patients $patient)
    {
        return view('show', compact('patient'));
    }

    public function edit(Patients $patient)
    {
        return view('edit', compact('patient'));
    }

    public function update(PatientStoreRequest $request, Patients $patient)
    {
        $validatedAttributes = $request->validated();

        $patient->update($validatedAttributes);
        return redirect()->route('edit', [$patient])->with('status', 'Data updated');
    }

    public function destroy(Patients $patient)
    {
        $patient->delete();
        return back()->with('status', 'Data is deleted');
    }
}
